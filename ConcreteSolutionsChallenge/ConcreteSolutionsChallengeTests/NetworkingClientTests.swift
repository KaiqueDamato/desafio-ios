//
//  NetworkingClientTests.swift
//  ConcreteSolutionsChallenge
//
//  Created by Kaique Damato on 9/23/16.
//  Copyright © 2016 Kaique Damato. All rights reserved.
//

import XCTest
@testable import ConcreteSolutionsChallenge

class MockHTTPClient: HTTPClient {
    var getCalled = false
    var urlPassed: URL?
    
    override func get(url: URL, parameters: [String : Any]?, completionHandler: GetReposCompletionHandler?) {
        getCalled = true
        urlPassed = url
    }
}

class NetworkingClientTests: XCTestCase {
    
    func testGetRepos() {
        //arrange
        let networkingClient = NetworkingClient()
        let httpClient = MockHTTPClient()
        let url = URL(string: "https://api.github.com/search/repositories")!
        
        networkingClient.httpClient = httpClient
        
        // act
        networkingClient.getJSON(from: url, page: 1, completionHandler: nil)
        
        //assert
        XCTAssert(httpClient.urlPassed == URL(string:"https://api.github.com/search/repositories"), "Não chamou a url certa")
        XCTAssert(httpClient.getCalled, "Não chamou o get")
    }
}
