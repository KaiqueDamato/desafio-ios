//
//  PullRequestViewController.swift
//  ConcreteSolutionsChallenge
//
//  Created by Kaique Damato on 9/24/16.
//  Copyright © 2016 Kaique Damato. All rights reserved.
//

import UIKit
import SafariServices

class PullRequestViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, PullRequestStoreDelegate, SFSafariViewControllerDelegate {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    var repository: Repository?
    
    let pullRequestStore = PullRequestStore()
    
    let networkingClient = NetworkingClient()
    let httpClient = HTTPClient()
    
    var safariViewController: SFSafariViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pullRequestStore.delegate = self
        safariViewController?.delegate = self
        
        networkingClient.httpClient = httpClient
        
        activityIndicator.startAnimating()
        tableView.separatorStyle = .none
        
        self.title = repository?.repositoryName
        
        guard let repo = repository else {
            return
        }
        
        pullRequestStore.getPullRequests(from: repo)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UIApplication.shared.statusBarStyle = .lightContent
        
        guard let indexPath = self.tableView.indexPathForSelectedRow else {
            return
        }
        
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if pullRequestStore.count != 0 {
            tableView.separatorStyle = .singleLine
        }
        return pullRequestStore.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PullRequestTableViewCell
        
        let pullRequest = pullRequestStore.getPullRequest(at: indexPath.row)
        
        cell.dateLabel.text        = pullRequest.date
        cell.titleLabel.text       = pullRequest.title
        cell.usernameLabel.text    = pullRequest.username
        cell.descriptionLabel.text = pullRequest.body
        
        DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
            let url = URL(string: pullRequest.avatar)!
            self.networkingClient.getAvatarImage(url: url, completionHandler: { (data, success) in
                if success {
                    if let image = UIImage(data: data!) {
                        cell.pictureImageView.backgroundColor = UIColor.white
                        cell.pictureImageView.image = image
                        cell.pictureImageView.contentMode = .scaleAspectFit
                    }
                }
            })
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let pullRequest = pullRequestStore.getPullRequest(at: indexPath.row)
        openPullRequest(url: pullRequest.url)
    }
    
    func refreshCell() {
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
            self.activityIndicator.hidesWhenStopped = true
            self.tableView.reloadData()
        }
    }
    
    func showAlert(text: String) {
        let alert = UIAlertController(title: "Atenção", message: text, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default) { (action) in
            DispatchQueue.main.async {
                let _ = self.navigationController?.popViewController(animated: true)
            }
        }
        
        alert.addAction(action)
        self.present(alert, animated: true) {
            self.activityIndicator.stopAnimating()
            self.activityIndicator.hidesWhenStopped = true
        }
    }
    
    func openPullRequest(url: String) {
        if let urlObject = URL(string: url) {
            safariViewController = SFSafariViewController(url: urlObject, entersReaderIfAvailable: true)
            UIApplication.shared.statusBarStyle = .default
            present(safariViewController!, animated: true, completion: nil)
        }
    }
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}
