//
//  Repository.swift
//  ConcreteSolutionsChallenge
//
//  Created by Kaique Damato on 9/24/16.
//  Copyright © 2016 Kaique Damato. All rights reserved.
//

import Foundation
import SwiftyJSON

class Repository {
    
    let repositoryName       :   String
    let repositoryStars      :   Int
    let repositoryForks      :   Int
    let repositoryOwner      :   String
    let repositoryDescription:   String
    let repositoryOwnerAvatar:   String
    
    init(repositoryName: String, repositoryStars: Int, repositoryForks: Int, repositoryOwner: String, repositoryDescription: String, repositoryOwnerAvatar: String) {
        self.repositoryName        = repositoryName
        self.repositoryStars       = repositoryStars
        self.repositoryForks       = repositoryForks
        self.repositoryOwner       = repositoryOwner
        self.repositoryDescription = repositoryDescription
        self.repositoryOwnerAvatar = repositoryOwnerAvatar
    }
    
    class func build(json: JSON) -> Repository? {
        if let
            repositoryName            = json["name"].string,
            let repositoryStars       = json["stargazers_count"].int,
            let repositoryForks       = json["forks"].int,
            let repositoryOwner       = json["owner"]["login"].string,
            let repositoryDescription = json["description"].string,
            let repositoryOwnerAvatar = json["owner"]["avatar_url"].string {
            return Repository(repositoryName: repositoryName, repositoryStars: repositoryStars, repositoryForks: repositoryForks, repositoryOwner: repositoryOwner, repositoryDescription: repositoryDescription, repositoryOwnerAvatar: repositoryOwnerAvatar)
        }
        return nil
    }
}
