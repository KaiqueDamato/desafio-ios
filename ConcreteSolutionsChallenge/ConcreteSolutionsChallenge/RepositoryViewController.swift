//
//  ViewController.swift
//  ConcreteSolutionsChallenge
//
//  Created by Kaique Damato on 9/23/16.
//  Copyright © 2016 Kaique Damato. All rights reserved.
//

import UIKit
import Alamofire

class RepositoryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, RepositoryStoreDelegate {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    let repositoryStore = RepositoryStore.sharedInstance
    
    let networkingClient = NetworkingClient()
    let httpClient       = HTTPClient()
    
    var selectedRepository: Repository?
    
    var needsMoreData = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        networkingClient.httpClient = httpClient
        
        repositoryStore.delegate = self
        repositoryStore.getReposData(completion: nil)
        activityIndicator.startAnimating()
        tableView.separatorStyle = .none
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard let indexPath = tableView.indexPathForSelectedRow else {
            return
        }
        
        tableView.deselectRow(at: indexPath, animated: animated)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if repositoryStore.count != 0 {
            tableView.separatorStyle = .singleLine
        }
        return repositoryStore.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! RepositoryTableViewCell
        
        let repository = repositoryStore.getRepo(index: indexPath.row)
        
        cell.nameLabel.text = repository.repositoryName
        cell.forksLabel.text = String(repository.repositoryForks)
        cell.descriptionLabel.text = repository.repositoryDescription
        cell.starsLabel.text = String(repository.repositoryStars)
        cell.usernameLabel.text = repository.repositoryOwner
        
        DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
            let url = URL(string: repository.repositoryOwnerAvatar)!
            self.networkingClient.getAvatarImage(url: url, completionHandler: { (data, success) in
                if success {
                    if let image = UIImage(data: data!) {
                        cell.pictureImageView.image = image
                        cell.pictureImageView.contentMode = .scaleAspectFit
                        cell.pictureImageView.backgroundColor = UIColor.white
                    }
                }
            })
        }
        
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if(tableView.contentOffset.y >= (tableView.contentSize.height - tableView.frame.size.height) && repositoryStore.count != 0 && needsMoreData) {
            needsMoreData = false
            DispatchQueue.main.async {
                self.repositoryStore.getReposData(completion: { (success) in
                    if !success {
                        self.showAlert()
                    }
                })
                let act = UIActivityIndicatorView(activityIndicatorStyle: .gray)
                act.startAnimating()
                act.frame = CGRect(x: 0, y: 0, width: 320, height: 44)
                self.tableView.tableFooterView = act
            }
        }
    }
    
    func refreshCell() {
        needsMoreData = true
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
            self.activityIndicator.isHidden = true
            self.tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedRepository = repositoryStore.getRepo(index: indexPath.row)
        performSegue(withIdentifier: "showPullRequests", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? PullRequestViewController {
            vc.repository = selectedRepository
        }
    }
    
    func showAlert() {
        UIView.animate(withDuration: 0.4, animations: {
            self.tableView.tableFooterView = nil
        })
        
        let alert = UIAlertController(title: "Atenção", message: "Você atingiu o limite de requisições, aguarde alguns instantes.", preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default) { (action) in
            self.needsMoreData = true
        }
        
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
}

