//
//  PullRequest.swift
//  ConcreteSolutionsChallenge
//
//  Created by Kaique Damato on 9/24/16.
//  Copyright © 2016 Kaique Damato. All rights reserved.
//

import Foundation
import SwiftyJSON

class PullRequest {
    
    let url     : String
    let body    : String
    let date    : String
    let title   : String
    let avatar  : String
    let username: String
    
    init(title: String, body: String, date: String, username: String, avatar: String, url: String) {
        self.body     = body
        self.date     = date
        self.title    = title
        self.avatar   = avatar
        self.username = username
        self.url      = url
    }
    
    class func build(json: JSON) -> PullRequest? {
        if let
            body         = json["body"].string,
            var date     = json["created_at"].string,
            let title    = json["title"].string,
            let avatar   = json["user"]["avatar_url"].string,
            let username = json["user"]["login"].string,
            let url      = json["html_url"].string {
            date.removeSubrange(date.index(date.startIndex, offsetBy: 10)..<date.endIndex)
            
            let dateFormatter = DateFormatter()
            
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let dateObject = dateFormatter.date(from: date)
            
            dateFormatter.dateFormat = "dd/MM/yyyy"
            let finalDate = dateFormatter.string(from: dateObject!)
            
            return PullRequest(title: title, body: body, date: finalDate, username: username, avatar: avatar, url: url)
        }
        return nil
    }
}
