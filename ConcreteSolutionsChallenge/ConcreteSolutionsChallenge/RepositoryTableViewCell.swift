//
//  RepositoryTableViewCell.swift
//  ConcreteSolutionsChallenge
//
//  Created by Kaique Damato on 9/24/16.
//  Copyright © 2016 Kaique Damato. All rights reserved.
//

import UIKit

class RepositoryTableViewCell: UITableViewCell {
    
    @IBOutlet var pictureImageView: KDImageView!
    @IBOutlet var usernameLabel: UILabel!
    @IBOutlet var forksLabel: UILabel!
    @IBOutlet var starsLabel: UILabel!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
