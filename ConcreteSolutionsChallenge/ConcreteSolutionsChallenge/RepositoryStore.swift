//
//  RepositoryStore.swift
//  ConcreteSolutionsChallenge
//
//  Created by Kaique Damato on 9/24/16.
//  Copyright © 2016 Kaique Damato. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol RepositoryStoreDelegate: NSObjectProtocol {
    func refreshCell()
}

class RepositoryStore: NSObject, RepositoryStoreDelegate {
    
    static let sharedInstance = RepositoryStore()
    weak var delegate: RepositoryStoreDelegate?
    
    private var currentPage = 1
    
    let url = URL(string: "https://api.github.com/search/repositories")!
    
    private var repositories = [Repository]() {
        didSet {
            refreshCell()
        }
    }
    
    private let networkingClient = NetworkingClient()
    private let httpClient = HTTPClient()
    
    private var dataFromHTTPClient: Data? {
        didSet {
            parseDataToJSON()
        }
    }
    
    var count: Int {
        get {
            return repositories.count
        }
    }
    
    func getRepo(index: Int) -> Repository {
        return repositories[index]
    }
    
    func getReposData(completion: ((_ success: Bool) -> Void)?) {
        networkingClient.httpClient = httpClient
        networkingClient.getJSON(from: url, page: currentPage) { (data, success) in
            if success {
                self.dataFromHTTPClient = data
                self.currentPage += 1
            } else {
                completion!(false)
            }
        }
    }
    
    private func parseDataToJSON() {
        let json = JSON(data: dataFromHTTPClient!)
        
        if let repositories = json["items"].array {
            for repository in repositories {
                if let repositoryObject = Repository.build(json: repository) {
                    self.repositories.append(repositoryObject)
                }
            }
        }
    }
    
    func refreshCell() {
        delegate?.refreshCell()
    }
}
