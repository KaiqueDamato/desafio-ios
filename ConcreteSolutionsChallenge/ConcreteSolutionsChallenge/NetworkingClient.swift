//
//  NetworkingClient.swift
//  ConcreteSolutionsChallenge
//
//  Created by Kaique Damato on 9/23/16.
//  Copyright © 2016 Kaique Damato. All rights reserved.
//

import Foundation
import Alamofire

typealias GetReposCompletionHandler = (_ repositories: Data?, _ success: Bool) -> Void
typealias GetAvatarImageCompletionHandler = (_ image: Data?, _ success: Bool) -> Void

class NetworkingClient {
    
    var httpClient: HTTPClientProtocol?
    var parameters: [String: Any]?
    
    func getJSON(from url: URL, page: Int?, completionHandler: GetReposCompletionHandler?) {
        if let pg = page {
            parameters = [
                "q": "language:Swift",
                "sort": "stars",
                "page": pg
            ]
        }
        
        httpClient?.get(url: url, parameters: parameters, completionHandler: completionHandler)
    }
    
    func getAvatarImage(url: URL, completionHandler: GetAvatarImageCompletionHandler?) {
        httpClient?.get(url: url, parameters: nil, completionHandler: completionHandler)
    }
}
