//
//  PullRequestTableViewCell.swift
//  ConcreteSolutionsChallenge
//
//  Created by Kaique Damato on 9/24/16.
//  Copyright © 2016 Kaique Damato. All rights reserved.
//

import UIKit

class PullRequestTableViewCell: UITableViewCell {
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var pictureImageView: KDImageView!
    @IBOutlet var usernameLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    
}
