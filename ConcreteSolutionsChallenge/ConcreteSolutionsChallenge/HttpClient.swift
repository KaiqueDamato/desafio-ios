//
//  HttpClient.swift
//  ConcreteSolutionsChallenge
//
//  Created by Kaique Damato on 9/24/16.
//  Copyright © 2016 Kaique Damato. All rights reserved.
//

import Foundation
import Alamofire

protocol HTTPClientProtocol {
    func get(url: URL, parameters: [String: Any]?, completionHandler: GetReposCompletionHandler?)
}

class HTTPClient: HTTPClientProtocol {
    
    internal func get(url: URL, parameters: [String : Any]?, completionHandler: GetReposCompletionHandler?) {
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil)
                    .validate()
                    .responseData { (response) in
                        if response.result.isSuccess {
                            completionHandler!(response.data, true)
                        } else {
                            completionHandler!(nil, false)
                        }
        }
    }
    
}
