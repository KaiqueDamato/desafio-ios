//
//  PullRequestStore.swift
//  ConcreteSolutionsChallenge
//
//  Created by Kaique Damato on 9/24/16.
//  Copyright © 2016 Kaique Damato. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol PullRequestStoreDelegate: NSObjectProtocol {
    func refreshCell()
    func showAlert(text: String)
}

class PullRequestStore: NSObject, PullRequestStoreDelegate {
    
    weak var delegate: PullRequestStoreDelegate?
    
    private let networkingClient = NetworkingClient()
    private let httpClient = HTTPClient()
    
    private var pullRequests = [PullRequest]() {
        didSet {
            refreshCell()
        }
    }
    
    private var dataFromHTTPClient: Data? {
        didSet {
            parseDataToJSON()
        }
    }
    
    var count: Int {
        get {
            return pullRequests.count
        }
    }
    
    func getPullRequest(at index: Int) -> PullRequest {
        return pullRequests[index]
    }
    
    func getPullRequests(from repository: Repository) {
        networkingClient.httpClient = httpClient
        
        if let url = URL(string: "https://api.github.com/repos/\(repository.repositoryOwner)/\(repository.repositoryName)/pulls") {
            networkingClient.getJSON(from: url, page: nil) { (data, success) in
                if success {
                    self.dataFromHTTPClient = data
                }
            }
        }
    }
    
    private func parseDataToJSON() {
        let json = JSON(data: dataFromHTTPClient!)
        
        if let pullRequests = json.array {
            if pullRequests.count == 0 {
                showAlert(text: "Não há pull requests nesse repositório")
            }
            
            for pullRequest in pullRequests {
                if let pullRequestObject = PullRequest.build(json: pullRequest) {
                    self.pullRequests.append(pullRequestObject)
                } else {
                    print("Não foi possível fazer o parse do objeto")
                }
            }
        }
    }
    
    internal func refreshCell() {
        delegate?.refreshCell()
    }
    
    func showAlert(text: String) {
        delegate?.showAlert(text: text)
    }
}
